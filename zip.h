#pragma once

#include <iterator>
#include <forward_list>
#include <string>

template <typename T1, typename T2>
class Zipped {

public:
    class ZippedIterator {
    public:
        ZippedIterator(std::pair<T1, T2> iter) {
            it_ = std::move(iter);
        }

        ZippedIterator& operator++() {
            ++this->it_.first;
            ++this->it_.second;
            return *this;
        }

        auto operator*() {
            return std::make_pair(*this->it_.first, *this->it_.second);
        }

        bool operator!=(const ZippedIterator& other) {
            return (this->it_.second != other.it_.second) && (this->it_.first != other.it_.first);
        }

    private:
        std::pair<T1, T2> it_;
    };

    Zipped(std::pair<T1, T2> begin, std::pair<T1, T2> end) {
        begin_ = std::move(begin);
        end_ = std::move(end);
    }

    ZippedIterator begin() {
        return ZippedIterator(begin_);
    }

    ZippedIterator end() {
        return ZippedIterator(end_);
    }

private:
    std::pair<T1, T2> begin_;
    std::pair<T1, T2> end_;
};

template <typename Sequence1, typename Sequence2>
auto Zip(const Sequence1& sequence1, const Sequence2& sequence2) {
    return Zipped(std::make_pair(std::begin(sequence1), std::begin(sequence2)),
                  std::make_pair(std::end(sequence1), std::end(sequence2)));
}

